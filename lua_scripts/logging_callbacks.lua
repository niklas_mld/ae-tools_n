--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- logging_callbacks.lua
-- Handles all the logging of event callbacks

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local force = require "lua_scripts.force"
local events = require "lua_scripts.custom_events"

-- TODO: Should perhaps be within manpower folder to ensure all lib code works wihout requiring feature-specific folders
local pop = require "lua_scripts.manpower.population_manager"

-- Callbacks
-- Each logging level stacks: level 3 includes level 2 and level 1 callbacks.
function characterDebugInfo(context)
    local character = context:character()

    dev.log("Character info:")
    dev.log("\tName: " .. character:get_forename() .. " " .. character:get_surname())
    dev.log("\tCQI: " .. character:cqi())
    dev.log("\tAge: " .. character:age())
    dev.log("\tFaction: " .. character:faction():name())
    dev.log("\tLogical Position: " .. character:logical_position_x() .. ", " .. character:logical_position_y())
    dev.log("\tDisplay Position: " .. character:display_position_x() .. ", " .. character:display_position_y() .. "\n")

    if character:has_military_force() then
        dev.log("\t--Military Force Data--")

        local forceCQI = character:military_force():command_queue_index()
        dev.log("\tForce CQI: " .. forceCQI)
        dev.log("\tTurns force has been in foreign land: " .. force.getForceTurnsInForeignRegion(forceCQI) .. "\n")
        dev.log( "\tAvailable Real Population: " .. pop.getTotalPopulation(pop.getPopulationSource(forceCQI)) )
        dev.log( "\tAvailable UI Population: " .. pop.UIgetTotalPopulation(pop.getPopulationSource(forceCQI)) )
    end
end

function settlementDebugInfo(context)
    local region = context:garrison_residence():region()
    local settlement = region:settlement()
    
    dev.log("Settlement info:")
    dev.log( "\tName: " .. region:name() )
    dev.log( "\tReal Population: " .. pop.getTotalPopulation(region:name()) )
    dev.log( "\tUI Population: " .. pop.UIgetTotalPopulation(region:name()) )
    dev.log( "\tFaction: " .. context:garrison_residence():faction():name() )
    dev.log( "\tLogical Position: " .. settlement:logical_position_x() .. ", " .. settlement:logical_position_y() )
    dev.log( "\tDisplay Position: " .. settlement:display_position_x() .. ", " .. settlement:display_position_y() .. "\n" )
end

-- Debug level 1 (Low)
if tonumber(dev.settings["debugLevel"]) >= 1 then
    events.AddEventCallBack("LoadingGame", function() dev.log("Loading game...") end)
    events.AddEventCallBack("WorldCreated", function() dev.log("\n---------------\nWorld created!\n---------------\n") end)
end

-- Debug level 2 (Med)
if tonumber(dev.settings["debugLevel"]) >= 2 then
    --events.AddEventCallBack("BattleCompleted", function() dev.log("Battle completed") end)
end

-- Debug level 3 (All)
if tonumber(dev.settings["debugLevel"]) >= 3 then
    events.AddEventCallBack("CharacterSelected", function() dev.log("Character selected") end)
    events.AddEventCallBack("CharacterSelected", characterDebugInfo)
    
    events.AddEventCallBack("SettlementSelected", function() dev.log("Settlement selected") end)
    events.AddEventCallBack("SettlementSelected", settlementDebugInfo)
end

-- Logging
dev.log("logging_callbacks.lua loaded")