--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- year_events.lua
-- Logic to call functions on a particular year
-- This is used for historical events, migrations, etc

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local lib = require "scripts_libs_mod.lib_year_events"
local events = require "lua_scripts.custom_events"
local world = require "lua_scripts.world"

-- This isn't a custom event because it's the reversed flow from normal callbacks
-- Instead of adding a callback and checking the year, you assign a function to a year and this functions grabs the callback
-- It's not too much of an important distinction, but makes things a little easier for non-scripters to add yearly event messages
function handleEvents()
    if not world.isPlayerTurn() then return end
    
    local currentTurn = world.getTurnNumber()
    local currentYear = world.convertTurnToYear(currentTurn)
    
    local funcTable = lib.events[currentYear] or {}
    
    for key, func in pairs(funcTable) do
        func(currentYear) -- We call each function in the callback list
        -- We pass in currentYear as an argument so that these functions know what year they are called from, which can be useful
    end
end

-- Callbacks
events.AddEventCallBack("FactionTurnStart", handleEvents)

-- Logging
dev.log("year_events.lua loaded")