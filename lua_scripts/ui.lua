--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- ui.lua
-- Handles user interface related modifications

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"

function resizeUIC(uic, x, y)
    uic:SetMoveable(true)
    uic:Resize(x, y)
    uic:SetMoveable(false)
end

function moveUIC(uic, x, y)
    uic:SetMoveable(true)
    uic:MoveTo(x, y)
    uic:SetMoveable(false)
end

function shiftUIC(uic, x, y)
    local pX, pY = uic:Position()
    uic:SetMoveable(true)
    uic:MoveTo(pX + x, pY + y)
    uic:SetMoveable(false)
end

function removeUIComponent(id)
    if id then
        local parent = UIComponent(id):Parent()

        UIComponent(id):SetDisabled(true)
        UIComponent(parent):Divorce(id)
    end
end

function logAllUIComponentInfo(uic)
    output_uicomponent(uic)
    for i = 0, uic:ChildCount()-1 do
        logAllUIComponentInfo(UIComponent(uic:Find(i)))
    end
    
    --dev.log("UIComponent...")
    --dev.log("\tName: " .. comp:Id())
    --dev.log("\tState text: " .. comp:GetStateText())
    --dev.log("\tState text details: " .. comp:GetStateTextDetails())
    --dev.log("\tTooltip text: " .. comp:GetTooltipText())
    --dev.log("\tCurrent state: " .. comp:CurrentState())
end

-- create and return a ui component
-- parent is the parent component, "kept" is the name of the UIC you want to keep from the file
-- name is the created component's custom name, and filepath is the path to the UIC definition file
-- if "kept" is empty, then we create the component without deleting anything
function createUIComponent(parent_uic, kept, name, filepath)
    parent_uic:CreateComponent(name, filepath)
    local childCount = parent_uic:ChildCount()
    
    -- Find the created component
    local uic = nil
    for i = 0, childCount - 1 do
        local child = parent_uic:Find(i)
        if UIComponent(child):Id() == name then
            uic_id = child
            break
        end
    end
    
    local uic = UIComponent(uic_id)
    
    if kept ~= "" then
        kept_uic_id = uic:Find(kept)
        kept_uic = UIComponent(kept_uic_id)
        
        if not kept_uic then
            dev.log("ERROR: createUIComponent()\n    UIC ''"..tostring(kept).."'' not found")
            uic:DestroyChildren()
            removeUIComponent(uic_id)
            return false
        end
        
        -- Divorce the kept UIC
        local kept_parent_uic = UIComponent(kept_uic:Parent())
        kept_parent_uic:Divorce(kept_uic_id)
        
        -- Destroy everything else
        uic:DestroyChildren()
        
        -- Adopt the kept UIC at the root
        uic:Adopt(kept_uic_id)
    end
    
    return uic
end

function textColourer(r, g, b, a)
    -- default is max opacity
    a = a or 255

    local colourer = function(text)
        local startTag = "[[rgba:".. r ..":".. g ..":".. b ..":" .. a .. "]]"
        local endTag =  "[[/rgba:".. r ..":".. g ..":".. b ..":" .. a .. "]]"
        return startTag .. text .. endTag
    end
    
    return colourer
end

-- Callbacks
--events.AddEventCallBack("ComponentLClickUp", function(context) output_uicomponent(UIComponent(context.component)) end)

dev.log("ui.lua loaded")