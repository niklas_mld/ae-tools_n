--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- world.lua
-- Handles world info

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local scripting = require "lua_scripts.EpisodicScripting"
require "worldParams"

function getStartYear()
    return startYear
end

function getTurnsPerYear()
    return turnsPerYear
end

function getTurnNumber()
    return scripting.game_interface:model():turn_number()
end

function getYear()
    return ((scripting.game_interface:model():turn_number() / turnsPerYear) + startYear) - 1/turnsPerYear
end

function convertYearToTurnRaw(year) -- Returns raw value - performs no checking to ensure turn number is an integer
    return ((year - startYear) * turnsPerYear) + 1/turnsPerYear
end

-- This next function needs some explaining - we want turn numbers to be ceiled up to the next turn, but also account for floating point errors
-- If errors cause a number 1 to be represented as 1.0000000000001, you don't want it ceiled up to 2, instead you want it rounded down
-- If you don't account for this, turn numbers can be wrongly pushed forwards a turn.

function convertYearToTurn(year) -- Returns value, accounting for float errors and ceiling number to next integer
    local turn = convertYearToTurnRaw(year)
    -- Check for floating point error by comparing "distance" against value rounded to closest int. 
    -- HUUUGE epsilon here (0.01) but unless you have very unreasonable turns per year it doesn't matter
    if (math.abs(turn - math.floor(turn + 0.5)) < 0.01) then
        return math.floor(turn + 0.5) -- If there is rounding errors, just round towards closest int
    else
        return math.ceil(turn) -- If not, ceil number to next int
    end
end

function convertTurnToYear(turn)
    return ((turn / turnsPerYear) + startYear) - 1/turnsPerYear
end

function isPlayerTurn()
    return scripting.game_interface:model():is_player_turn()
end