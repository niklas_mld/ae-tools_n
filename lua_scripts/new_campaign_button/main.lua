--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- main.lua
-- Handles main menu user interface related modifications for new campaign button

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local ui = require "lua_scripts.ui"
local scripting = require "lua_scripts.EpisodicScripting"
local events = require "lua_scripts.custom_events"

function moveCustomCampaignButton(context)
    if context.string == "button_campaign" then
        local button_mod_campaign_uic = UIComponent(scripting.m_root:Find("button_mod_campaign"))
        ui.shiftUIC(button_mod_campaign_uic, 0, 60*4)
    end
end

-- Callbacks
-- Disabled testing stuff
--events.AddEventCallBack("UICreated", moveCustomCampaignButton)
events.AddEventCallBack("ComponentMouseOn", moveCustomCampaignButton)
--events.AddEventCallBack("ComponentLClickUp", function(context) ui.logAllUIComponentInfo(UIComponent(context.component)) end)

events.AddEventCallBack("ComponentMouseOn", function(context)
    UIComponent(scripting.m_root:Find("button_purchase")):SetVisible(false) 
    UIComponent(scripting.m_root:Find("button_start_campaign")):SetVisible(true)
end)

dev.log("new_campaign_button loaded")