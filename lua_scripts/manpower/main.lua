--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- main.lua
-- A manpower system to limit recruitment of troops

-- WIP.

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"
local util = require "lua_scripts.util"
local cond = require "lua_scripts.condition"
local ui_recruitment = require "lua_scripts.ui_recruitment"

local unit_interface = require "lua_scripts.manpower.unit_interface"
local manpower_ui = require "lua_scripts.manpower.ui"
local state_manager = require "lua_scripts.manpower.force_state_manager"
local pop = require "lua_scripts.manpower.population_manager"

-- Saved / loaded stuff
m_newGame = true -- Is this a new game / first time the mod has been run on this save?

m_recruitmentOrders = {} -- Stores the recruitment orders per character, so that we can give population back if an order is cancelled
m_currentCharCQI = nil -- Stores current character, so the UI callback knows which character's UI is open

function initManpower()
    dev.log("Initializing the manpower system")
    pop.initializePopulationSources()

    if m_newGame then
        m_newGame = false
        manpower_ui.showManpowerIntroToAllFactions()
    end
end

function safeCheckForRestrictedUnits()
    if not m_currentCharCQI then return end
    local currentCharacter = cm:model():character_for_command_queue_index(m_currentCharCQI)
    local forceCQI = currentCharacter:military_force():command_queue_index()
    local popSourceKey = pop.getPopulationSource(forceCQI)
    
    checkForRestrictedUnits(popSourceKey)
end

function getUnitRestrictions(popSourceKey)
    local units = {}
    
    local list_box_uic = UIComponent( scripting.m_root:Find("list_box") )
    if not list_box_uic then return end
    
    local recruitmentCardCount = list_box_uic:ChildCount()
    local recruitmentCards = {}

    for i = 0, recruitmentCardCount - 1 do
        local recruitmentCardKey = UIComponent(list_box_uic:Find(i)):Id()
        
        if ui_recruitment.getRecruitableType(recruitmentCardKey) == "faction" then
            recruitmentCards[i] = recruitmentCardKey
        end
    end

    for list_box_child_id, recruitmentCardKey in pairs(recruitmentCards) do
        local unitKey = ui_recruitment.getUnitKeyFromRecruitable(recruitmentCardKey)
        local unitSize = unit_interface.getUnitSize(unitKey)
        local manpower = pop.UIgetManpowerForUnit(popSourceKey, unitKey)

        units[list_box_child_id] = (unitSize <= manpower)
    end
    
    return units
end

function checkForRestrictedUnits(popSourceKey)
    local units = getUnitRestrictions(popSourceKey)
    local list_box_uic = UIComponent( scripting.m_root:Find("list_box") )
    
    -- We don't always technically need to set unit cards as enabled, as they usually already are,
    -- and the recruitment UI updates itself on most events anyways
    -- but that doesn't work with slightly delayed events such as disbanding
    
    for list_box_child_id, canRecruit in pairs(units) do
        local list_box_child_uic = UIComponent(list_box_uic:Find(list_box_child_id))
        if canRecruit then
            manpower_ui.enableRecruitment(list_box_child_uic, popSourceKey)
        else
            manpower_ui.disableRecruitment(list_box_child_uic, popSourceKey)
        end
    end
end

local function checkForceChanged(forceCQI)
    local isForce = cm:model():has_military_force_command_queue_index(forceCQI)
    local currentForceTable = nil
    if isForce then
        local force = cm:model():military_force_for_command_queue_index(forceCQI)
        currentForceTable = state_manager.createForceList(force)
    else
        currentForceTable = {}
    end

    return state_manager.areForcesDifferent(m_disbandForceTable, currentForceTable)
end

local function performUIPopUpdate(forceCQI, popSourceKey)
    local isForce = cm:model():has_military_force_command_queue_index(forceCQI)
    local currentForceTable = nil
    if isForce then
        local force = cm:model():military_force_for_command_queue_index(forceCQI)
        currentForceTable = state_manager.createForceList(force)
    else
        currentForceTable = {}
    end
    
    -- I should split up force_state_manager so it can work better for UI too
    local difference = state_manager.getForceDifference(currentForceTable, m_disbandForceTable)
                
    for unitKey, tab in pairs(difference) do
        local soldierCount = tab.soldierCount
        if soldierCount < 0 then -- Not really needed, but stops debug message spam
            pop.UIaddManpowerForUnit(popSourceKey, -soldierCount, unitKey)
        end
    end
    
    checkForRestrictedUnits(popSourceKey)
    
    m_disbandForceTable = nil
end

-- We hold the character cqi so we can find out what character the player is attempting to recruit units through
local function CharacterSelected(context)
    m_currentCharCQI = context:character():cqi()
end

m_recruitingCard = nil
m_recruitingCharacter = nil
local function unitRecruitmentCardClicked(recruitmentCard)
    local currentCharacter = cm:model():character_for_command_queue_index(m_currentCharCQI)
    local forceCQI = currentCharacter:military_force():command_queue_index()
    local recruitableType = ui_recruitment.getRecruitableType(recruitmentCard)

    -- Set m_recruitingCard so RecruitmentItemIssuedByPlayer can use it
    -- We need to do this just in case the player can click on another character
    -- before the recruitment order actually happens, which could be likely in MP
    -- Only set it if it's not nil though! The first click is the most important
    -- The later ones are ignored by the game in mp if it's lagging. I think. I hope
    if recruitableType == "faction" then -- Levies and mercs take no manpower
        m_recruitingCard = m_recruitingCard or recruitmentCard
        m_recruitingCharacter = m_recruitingCharacter or currentCharacter
    end
end

local function unitRecruitmentCardCancelled(recruitmentCard)
    local currentCharacter = cm:model():character_for_command_queue_index(m_currentCharCQI)
    local forceCQI = currentCharacter:military_force():command_queue_index()
    local popSourceKey = pop.getPopulationSource(forceCQI)

    -- We must check that it is QueuedLandUnit so that temp_merc doesn't get through
    local isCancellingRecruitmentOrder = string.find(recruitmentCard, "QueuedLandUnit ")
    if isCancellingRecruitmentOrder then
        local queueID = tonumber(string.sub(recruitmentCard, 16)) + 1 -- +1 because lua tables aren't 0-indexed

        local queuedUnitKey = m_recruitmentOrders[forceCQI][queueID]
        local queuedUnitSize = unit_interface.getUnitSize(queuedUnitKey)
        
        table.remove(m_recruitmentOrders[forceCQI], queueID)
        pop.UIaddManpowerForUnit(popSourceKey, queuedUnitSize, queuedUnitKey) -- Add the population back
        
        scripting.game_interface:add_time_trigger("checkRestrictedUnits", 0.0) -- Must delay to next frame, when game updates unit card
    end
end

local function checkForDisbanding(uicClicked)
    if uicClicked == "button_tick" then -- Player has clicked to disband units
        dev.log("disbanding confirmed")
        
        local currentCharacter = cm:model():character_for_command_queue_index(m_currentCharCQI)
        local forceCQI = currentCharacter:military_force():command_queue_index()
        local popSourceKey = pop.getPopulationSource(forceCQI)

        -- Need to wait for game to update forces
        cond.addCondition(
            function()
                return checkForceChanged(forceCQI)
            end,
            function()
                dev.log("force changed from disband, updating UI population")
                performUIPopUpdate(forceCQI, popSourceKey)
            end
        )

        m_inDisbandConfirm = false
    elseif uicClicked == "button_cancel" then -- Player has clicked cancel
        dev.log("disbanding cancelled")
        m_disbandForceTable = nil -- Forget about the force
        m_inDisbandConfirm = false
    end
end

m_inDisbandConfirm = false
m_disbandForceTable = nil

local function ComponentLClickUp(context)
    if not m_currentCharCQI then return end
    if not cm:model():has_character_command_queue_index(m_currentCharCQI) then return end

    local currentCharacter = cm:model():character_for_command_queue_index(m_currentCharCQI)
    local forceCQI = currentCharacter:military_force():command_queue_index()
    if not forceCQI then return end
    
    if ui_recruitment.isRecruitmentCard(context.string) and UIComponent(context.component):CurrentState() == "active" then 
        -- A unit has just been clicked on to be recruited
        unitRecruitmentCardClicked(context.string)
    end
    
    if ui_recruitment.isQueuedRecruitmentCard(context.string) then
        unitRecruitmentCardCancelled(context.string)
    end
    
    -- Technically checking for button clicks alone is a little unsafe as the player can exit the disband screen via pressing escape
    -- However, population should never be incorrectly changed due to it
    if context.string == "button_disband" then 
        m_inDisbandConfirm = true 
        dev.log("entering disband confirm screen")
        -- Save the state of the force, so we can query it after the disband to figure out changes
        m_disbandForceTable = state_manager.createForceList(currentCharacter:military_force())
    elseif m_inDisbandConfirm then
        -- We're already in the disbanding confirmation screen
        checkForDisbanding(context.string)
    end

    -- Note that the game re-opens the recruitment panel every time you click a unit
    -- So PanelOpened callback handles checking whether unit cards should be cancelled or not
end

local function TimeTrigger(context)
    if context.string == "checkRestrictedUnits" then
        safeCheckForRestrictedUnits()
    end
end

local function RecruitmentItemIssuedByPlayer(context)
    -- We shouldn't need to check for if m_recruitingCharacter or m_recruitingCard is nil, because this is only ever
    -- called if a recruitment is issued (which needs a character to be selected and for a unit card to be clicked)

    -- This is only called upon recruitment orders, not mercs or levies
    if not m_recruitingCard then return end

    local unitKey = ui_recruitment.getUnitKeyFromRecruitable(m_recruitingCard)
    local unitSize = unit_interface.getUnitSize(unitKey)
    local unitCulture = unit_interface.getUnitCulture(unitKey)
    local unitClass = unit_interface.getUnitClass(unitKey)

    dev.log("recruitment issued for " .. unitKey)
    
    local force = m_recruitingCharacter:military_force()
    local forceCQI = force:command_queue_index()
    local popSourceKey = pop.getPopulationSource(forceCQI)
    
    -- We don't need to check if there is enough population to recruit the unit, as it'll have already been disabled if so
    dev.log("Recruiting "..unitKey..", taking " .. unitSize .. " population from " .. unitCulture .. ": " .. unitClass)
    
    m_recruitmentOrders[forceCQI] = m_recruitmentOrders[forceCQI] or {}
    
    table.insert(m_recruitmentOrders[forceCQI], unitKey)
    pop.UIaddManpowerForUnit(popSourceKey, -unitSize, unitKey)
    
    m_recruitingCard = nil
    m_recruitingCharacter = nil
end

------------------------------------------------
---------------- Callbacks ----------------
------------------------------------------------

events.AddEventCallBack("WorldCreated", initManpower)

events.AddEventCallBack("CharacterSelected", CharacterSelected)
events.AddEventCallBack("ComponentLClickUp", ComponentLClickUp)
events.AddEventCallBack("RecruitmentItemIssuedByPlayer", RecruitmentItemIssuedByPlayer)
events.AddEventCallBack("TimeTrigger", TimeTrigger)

events.AddEventCallBack("PanelOpenedCampaign", function(context) -- These functions modify UI, so must be called only after the panels are created
    if context.string == "units_recruitment" then
        safeCheckForRestrictedUnits()
    end
end)

------------------------------------------------
---------------- Saving/Loading ----------------
------------------------------------------------

cm:register_loading_game_callback(
    function(context)
        m_newGame = cm:load_value("m_newGame", true, context)
        m_recruitmentOrders = util.deepLoadTable(context, "m_recruitmentOrders")
    end 
)

cm:register_saving_game_callback(
    function(context)
        cm:save_value("m_newGame", m_newGame, context)
        util.deepSaveTable(context, m_recruitmentOrders, "m_recruitmentOrders")
    end 
)

-- Logging
dev.log("manpower loaded")