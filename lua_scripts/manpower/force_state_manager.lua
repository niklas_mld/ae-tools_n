--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- force_state_manager.lua
-- A system which can track the changes performed to an force
-- Note: this doesn't track recruitment - that's handled by UnitCreated

-- This is used to determine when disbanding happens, as there is no callback for when a unit is disbanded
-- This also determines how many soldiers unit replenishment should take
-- We'll also use UI events to determine when the disband button is pressed, but that wouldn't be synced across MP (while this is)

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"
local util = require "lua_scripts.util"
local util_region = require "lua_scripts.region"
local util_faction = require "lua_scripts.faction"

local unit_interface = require "lua_scripts.manpower.unit_interface"
local pop = require "lua_scripts.manpower.population_manager"

-- Note, this method of doing things is very  hacky and weird, but it's forced if we want MP support

-- This could've all mostly been avoided by a few more callbacks...
-- CA, if you're reading, this is my requested list of callbacks:
--      BattleStarted (called when a battle is actually starting, not just on the pending screen where people can retreat) maybe PreBattle works here? not tested
--      CharacterRetreated or ForceRetreated. You have SiegeLifted, so why not this?!?
--      CharacterMovementOrder and CharacterHalted. This can be faked via collision zones or position polling, but I'd want this callback to be called before they actually begin moving
--      UnitReplenished
--      UnitDisbanded
--      RecruitmentItemRemovedByPlayer

m_forceState = {} -- Tracks forces state, so if there's a change between the current force and this we know there was a change
m_forceRegion = {} -- We must also store the region each force was in, in case it is disbanded

local function ScriptedForceCreated(context)
    -- If a scripted force is created, we don't want it demolishing the population of the region
    -- So we add the scripted force to our list and pretend it was here all along
    local force = context:military_force()
    local forceCQI = force:command_queue_index()
    m_forceState[forceCQI] = createForceList(force)
end

-- When a military force's unit count changes, update it
local function ForceUnitCountChanged(context)
    local forceCQI = context.forceCQI
    if not cm:model():has_military_force_command_queue_index(forceCQI) then return end

    -- ForceUnitCountChanged only calls for the force changes during their own turn
    -- so endTurn is false
    updateForce(forceCQI, false, context.regionKey)
end

-- When a military force moves, update it (to handle attrition)
local function CharacterMoved(context)
    local charCQI = context.charCQI

    if not cm:model():has_character_command_queue_index(charCQI) then return end

    local character = cm:model():character_for_command_queue_index(charCQI)
    if not character:has_military_force() then return end
    
    local forceCQI = character:military_force():command_queue_index()

    -- CharacterMoved only calls for the characters moved during their own turn
    -- so endTurn is false
    updateForce(forceCQI, false, context.regionKey)
end


local function ForcesMerge(context)
    local force1 = context:character():military_force()
    local force1CQI = force1:command_queue_index()
    
    local force2 = context:target_character():military_force()
    local force2CQI = force2:command_queue_index()
    
    -- Skip force updates, so updateForce doesn't notice any change from the merge and change population
    scripting.game_interface:add_time_trigger("forceMerged_" .. force1CQI, 0.0) -- Must delay to next frame, when game updates forces
    scripting.game_interface:add_time_trigger("forceMerged_" .. force2CQI, 0.0)
    
    dev.log("forces merged")
end

local duringBattleScene = false;

local function PendingBattle(context)
    -- Update both forces to check for disbanded units etc
    local battle = context:pending_battle()
    
    local attacker = battle:attacker()
    local defender = battle:defender()

    -- Globals, since we need this for after the battle too
    attackerForceCQI = attacker:military_force():command_queue_index()
    defenderForceCQI = defender:military_force():command_queue_index()

    -- It must be the attacking faction's turn
    updateForce(attackerForceCQI, false)
    updateForce(defenderForceCQI, true)

    duringBattleScene = true
end

local function BattleCompleted()
    scripting.game_interface:add_time_trigger("battleCompleted", 0.0)
end

local function TickAfterBattleCompleted()
    -- Update both forces, but ignore changes (since we don't want the dead soldiers to be fed back in region manpower)
    
    -- We need to access the forces via military_force_for_cqi, 
    -- instead of going through the context and characters directly
    -- because a character may have died in the battle
    
    -- If either of these are nil, the entire force has been wiped out
    local attackerForce = nil
    local defenderForce = nil
    
    if cm:model():has_military_force_command_queue_index(attackerForceCQI) then
        attackerForce = cm:model():military_force_for_command_queue_index(attackerForceCQI)
    end
    
    if cm:model():has_military_force_command_queue_index(defenderForceCQI) then
        defenderForce = cm:model():military_force_for_command_queue_index(defenderForceCQI)
    end
    
    -- Skip force update, so updateForce doesn't notice any losses from the battle and change population
    if attackerForce then
        m_forceState[attackerForceCQI] = createForceList(attackerForce)
    else
        m_forceRegion[attackerForceCQI] = nil
        m_forceState[attackerForceCQI] = nil

        -- We don't check that the force we are destroying is a horde, as it'd only destroy a non-existent pop source then
        pop.destroyPopulationSource(pop.getHordePopulationSource(attackerForceCQI))
    end
    
    if defenderForce then
        m_forceState[defenderForceCQI] = createForceList(defenderForce)
    else
        m_forceRegion[defenderForceCQI] = nil
        m_forceState[defenderForceCQI] = nil

        pop.destroyPopulationSource(pop.getHordePopulationSource(defenderForceCQI))
    end

    attackerForceCQI = nil
    defenderForceCQI = nil

    duringBattleScene = false;
end

-- Update the force when a unit was created
-- This is only really needed for if a levy is recruited (which is free in manpower) 
-- and then immediately disbanded before another force update occurs (from end turn, moving etc)
-- In which case the levy disbanding doesn't give manpower as it should
m_createdUnits = {}
local function UnitCreated(context)
    if util_faction.getCurrentFaction() then
        -- The context is broken, and trying to use the unit script interface crashes the game 
        -- This is as the function is called before the unit is actually created
        table.insert(m_createdUnits, context:unit())

        -- So we delay the update until next tick
        scripting.game_interface:add_time_trigger("unitCreated", 0.0)
    end
end

local function FactionTurnStart(context)
    -- Add all of our factions forces back to the list of tracked forces
    local forces = context:faction():military_force_list():num_items()
    for i = 0, forces - 1 do
        local force = context:faction():military_force_list():item_at(i)
        local forceCQI = force:command_queue_index()

        if not force:is_navy() then -- We don't support navies yet
            if not m_forceState[forceCQI] then m_forceState[forceCQI] = createForceList(force) end
            updateForce(forceCQI, true) -- Update the force, to take into account changes during end turn
        end
    end

    -- Need this to be done after all the forces are updated
    if context:faction():is_human() then -- Only humans care about UI
        pop.resetUIPopulation()
    end
end

local function FactionTurnEnd(context)
    --if not context:faction():is_human() then return end -- testing
    
    local forces = context:faction():military_force_list():num_items()
    for i = 0, forces - 1 do
        local force = context:faction():military_force_list():item_at(i)
        local forceCQI = force:command_queue_index()
        
        if not force:is_navy() then -- We don't support navies yet
            if not m_forceState[forceCQI] then m_forceState[forceCQI] = createForceList(force) end
            updateForce(forceCQI, false)
        end
    end
    
    for forceCQI, forceList in pairs(m_forceState) do
        local forceExists = cm:model():has_military_force_command_queue_index(forceCQI)
        if not forceExists then
            -- Entire force has disbanded! Feed population into region they disbanded in
            dev.log("force " .. forceCQI .. " has been completely disbanded!")
            
            for unitKey, tab in pairs(forceList) do
                local regionKey = m_forceRegion[forceCQI]

                -- Mercs don't take or give population when recruited/disbanded

                -- Levies are free to recruit but still give population when disbanded
                -- A levied troop represents a unit of already recruited men being given to the parent faction
                if not unit_interface.isMercenary(unitKey) then
                    local soldierCount = tab.soldierCount
                    pop.addManpowerForUnit(regionKey, soldierCount, unitKey)
                end
            end
            
            m_forceRegion[forceCQI] = nil
            m_forceState[forceCQI] = nil

            -- Also delete the forces population source
            -- We don't check that the force we are destroying is a horde, as it'd only destroy a non-existent pop source then
            pop.destroyPopulationSource(pop.getHordePopulationSource(forceCQI))
        end
    end

    -- Now we use m_forceState to determine changes which happen throughout the end turn sequence
end

-- Really complex edge case logic to try and determine the reason for force change
local function getChangeReason(unitKey, tab, factionKey, regionKey, endTurn)
    local soldierCount = tab.soldierCount
    local unitCount = tab.unitCount

    if soldierCount == 0 then
        -- this is an early exit optimization trick in common situation where there is no change
        -- and allows us to skip all the other checks
        return nil
    end

    local isMerc = unit_interface.isMercenary(unitKey)
    
    if endTurn then
        if soldierCount < 0 then -- Loss in soldier count. Must be attrition, as cannot disband during end-turn
            return "attrition"
        elseif soldierCount > 0 and not isMerc then -- Soldiers created during end-turn are factional, or replenishment
            return "recruit_factional/replenish_levy"
        elseif soldierCount > 0 and isMerc then -- Mercenary replenishment
            return "recruit_merc"
        end
    else
        -- If the force is in a non-owned territory, any units created must be levies or mercs
        local isLevyOrMercRecruited = not util_region.isOwnedRegion(regionKey, factionKey)

        -- However it could also be a merc recruited in local territory
        isLevyOrMercRecruited = isLevyOrMercRecruited or isMerc
        
        if soldierCount < 0 and unitCount >= 0 then -- No missing units, and loss in soldier count. Assume attrition
            return "attrition"
        elseif soldierCount < 0 and unitCount < 0 then -- A unit is missing, and the soldier count has dropped. Assume disbanding
            if isMerc then
                return "disband_merc"
            else
                return "disband_factional/disband_levy"
            end
        elseif soldierCount > 0 and isLevyOrMercRecruited then -- Merc or levy has been recruited
            return "recruit_merc/recruit_levy"
        elseif soldierCount > 0 then -- Force general has been created
            return "recruit_factional"
        end
    end
end

-- This function updates the state of an force, feeding any discrepancies (extra or missing units) into the population pool

-- Need to handle half-damaged units etc
-- Note this should only change population if either a unit is completely created/destroyed, like disbanded or levied troops
-- Or if the force units get bigger (due to replenishment). if the units get smaller, this is attrition, and must be ignored

-- endTurn is whether it is during the turn or the end turn sequence that the force is being updated
-- This is useful as levies, mercs are recruited during turn, and normal recruitment is just before the turn starts
function updateForce(forceCQI, endTurn, regionKey)
    -- If during a battle end, do not update any forces. wait until the scene is over
    -- This stops retreating force movements / take on warriors etc changing population
    if duringBattleScene then return end
    
    local force = cm:model():military_force_for_command_queue_index(forceCQI)
    
    local factionKey = force:faction():name()
    local character = force:general_character()
    
    if force:is_navy() or not character:has_region() then return end -- We don't support navies... yet
    
    -- regionKey is an optional argument. if it is not sent in, it uses the character's current region for population changes
    -- this allows the script to properly feed the population into the region where the force was last tick instead of it's current one
    -- used because the callback is slightly "late" due to being called only after changing
    -- (otherwise you can abuse region borders to trick the script what region you levied troops from or disbanded them, theoretically)
    local regionKey = regionKey or character:region():name()
    m_forceRegion[forceCQI] = regionKey

    local popSourceKey = pop.getPopulationSource(forceCQI, regionKey)
    
    local currentForceTable = createForceList(force)

    -- Sometimes this can be fed non-existent tables
    if not m_forceState[forceCQI] then
        if factionKey == util_faction.getCurrentFaction():name() then
            -- It's this factions turn, a new force has been created
            m_forceState[forceCQI] = {}
        else
            -- Not this faction's turn. Scripted created force or faction has not had a turn start yet
            m_forceState[forceCQI] = createForceList(force)
        end
    end

    local difference = getForceDifference(currentForceTable, m_forceState[forceCQI])
    
    for unitKey, tab in pairs(difference) do
        local changeReason = getChangeReason(unitKey, tab, factionKey, regionKey, endTurn)
        local soldierCount = tab.soldierCount

        -- Levies take population when replenishing but not when recruited
        -- This is because a levy is considered a military unit that the client already has that they are giving to their parent faction
        -- Maintaining that unit and replenishing it would be assumed to be from the general populace, unlike with mercenaries
        -- Effectively, we consider levies to be completely normal factional units except that they cost no manpower when initially recruited

        if changeReason == nil then
            -- Do nothing.
            -- this check is an optimization trick to avoid further checks down the elseif chain in common situation where no change happens
        elseif changeReason == "attrition" then
            -- and check attrition second as it's the most likely situation when an army is moving
            dev.log("Movement attrition in " .. unitKey .. ", killing " .. -soldierCount .. " soldiers")
            -- No population change
        elseif string.find(changeReason, "recruit_factional") or string.find(changeReason, "replenish_levy") then
            dev.log("Recruiting " .. unitKey .. ", taking " .. soldierCount .. " population from " .. popSourceKey)
            pop.addManpowerForUnit(popSourceKey, -soldierCount, unitKey)
        elseif string.find(changeReason, "disband_factional") or string.find(changeReason, "disband_levy") then
            dev.log("Disbanding " .. unitKey .. ", giving " .. -soldierCount .. " population to " .. popSourceKey)
            pop.addManpowerForUnit(popSourceKey, -soldierCount, unitKey)
        elseif string.find(changeReason, "recruit_merc") or string.find(changeReason, "recruit_levy") then
            dev.log("Recruited merc/levy " .. unitKey .. ". This is free in manpower")
            -- No population change
        elseif changeReason == "disband_merc" then
            dev.log("Disbanded merc " .. unitKey .. ". This gives no manpower")
            -- No population change
        end
    end
    
    m_forceState[forceCQI] = util.deepCopy(currentForceTable) -- Now update the force, because we've handled the disbanded/created troops
end

-- Returns the difference between 2 force tables. Can be used to figure out what units have disbanded, unit replenishment etc
function getForceDifference(force1_table, force2_table)
    local differenceTable = util.deepCopy(force1_table)
    
    for unit_key, tab in pairs(force2_table) do
        if not differenceTable[unit_key] then differenceTable[unit_key] = {} end
        if not differenceTable[unit_key].soldierCount then differenceTable[unit_key].soldierCount = 0 end
        if not differenceTable[unit_key].unitCount then differenceTable[unit_key].unitCount = 0 end
        differenceTable[unit_key].soldierCount = differenceTable[unit_key].soldierCount - tab.soldierCount
        differenceTable[unit_key].unitCount = differenceTable[unit_key].unitCount - tab.unitCount
    end
    
    return differenceTable
end

-- Returns whether 2 forces are different (for example, checking if an force has changed)
function areForcesDifferent(force1_table, force2_table)
    local difference = getForceDifference(force1_table, force2_table)

    for unit_key, tab in pairs(difference) do
        if tab.soldierCount ~= 0 or tab.unitCount ~= 0 then
            return true
        end
    end
    
    return false
end

function createForceList(force)
    local forceTable = {}
    local forceList = force:unit_list()
    
    for i = 0, forceList:num_items() - 1 do
        local unit = forceList:item_at(i)
        local unitKey = unit:unit_key()
        -- We need to do some maths here to figure out the unit count
        -- So we divide by 100, as it's a percentage
        -- Doing it in this order - multiplying them together BEFORE dividing by 100 -
        -- ensures the most accuracy out of the floating point calculations
        local unitSize = (unit_interface.getUnitSize(unitKey) * unit:percentage_proportion_of_full_strength()) / 100
        
        -- We still need to round it though, to catch tiny errors that get through:
        unitSize = math.floor(unitSize + 0.5) -- math.floor + 0.5 will round to the closest int
        
        if not forceTable[unitKey] then 
            forceTable[unitKey] = {} 
            forceTable[unitKey].soldierCount = 0
            forceTable[unitKey].unitCount = 0 
        end
        
        forceTable[unitKey].soldierCount = forceTable[unitKey].soldierCount + unitSize
        forceTable[unitKey].unitCount = forceTable[unitKey].unitCount + 1
    end
    
    return forceTable
end

local function TimeTrigger(context)
    if string.find(context.string, "forceMerged_") then
        local forceCQI = tonumber(string.gsub(context.string, "forceMerged_", "", 1))
        local force = cm:model():military_force_for_command_queue_index(forceCQI)
        m_forceState[forceCQI] = createForceList(force)
    elseif context.string == "unitCreated" then
        for i = 1, #m_createdUnits do
            local force = m_createdUnits[i]:military_force()
            local forceCQI = force:command_queue_index()
            
            local isEndTurn = force:faction():name() ~= util_faction.getCurrentFaction():name()
            updateForce(forceCQI, isEndTurn)
        end

        m_createdUnits = {}
    elseif context.string == "battleCompleted" then
        TickAfterBattleCompleted()
    end
end

------------------------------------------------
---------------- Callbacks ----------------
------------------------------------------------

events.AddEventCallBack("FactionTurnStart", FactionTurnStart)
events.AddEventCallBack("FactionTurnEnd", FactionTurnEnd)
events.AddEventCallBack("ScriptedForceCreated", ScriptedForceCreated)
events.AddEventCallBack("TimeTrigger", TimeTrigger)
events.AddEventCallBack("CharacterMoved", CharacterMoved)
events.AddEventCallBack("ForceUnitCountChanged", ForceUnitCountChanged)
events.AddEventCallBack("BattleCompleted", BattleCompleted)
events.AddEventCallBack("CampaignArmiesMerge", ForcesMerge)
events.AddEventCallBack("PendingBattle", PendingBattle)
events.AddEventCallBack("UnitCreated", UnitCreated)

------------------------------------------------
---------------- Saving/Loading ----------------
------------------------------------------------

cm:register_loading_game_callback(function(context) m_forceState = util.deepLoadTable(context, "m_forceState") end)
cm:register_saving_game_callback(function(context) util.deepSaveTable(context, m_forceState, "m_forceState") end)

-- Logging
dev.log("manpower.force_state_manager.lua loaded")