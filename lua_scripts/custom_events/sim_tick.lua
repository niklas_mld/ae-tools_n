--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- sim_tick.lua
-- Event which is called each campaign tick (10hz)

-- this line needs to be present - due to the way this script file is loaded by custom_events.lua it
-- does not have access to the global script environment. This line gives it access.
setfenv(1, _G.custom_events_env)

local dev = require "lua_scripts.dev"

createEvent("SimTick")

local function tickTrigger(context)
    if context.string == "tick_tock" then
        -- This is for the OnSimTick
        -- First we must set up a time trigger so this is called again ASAP
        scripting.game_interface:add_time_trigger("tick_tock", 0.0)
        -- Time to wait is 0.0 - but the game must defer it to the next tick
        -- According to my experimentation, the campaign runs at a logical tickrate of 10fps, so this function is called 10 times a second
        
        for key, func in getEventCallbacks("SimTick") do
            func()
        end
    end
end

AddEventCallBack("TimeTrigger", tickTrigger)
AddEventCallBack("WorldCreated", function() 
    scripting.game_interface:add_time_trigger("tick_tock", 0.0) 
end)

dev.log("custom_events.sim_tick.lua loaded")