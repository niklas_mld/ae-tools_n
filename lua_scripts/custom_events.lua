--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- custom_events.lua
-- Adds new events

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"

_callbacks = {}

function createEvent(event)
    _callbacks[event] = {}
end

function getEventCallbacks(event)
    return ipairs(_callbacks[event])
end

function AddEventCallBack(event, func)
    if _callbacks[event] then -- If this is a defined custom callback, add it
        table.insert(_callbacks[event], function(context) dev.logcall(function() func(context) end) end)
    else -- else add it to AddEventCallBack with dev.logcall for error checking
        scripting.AddEventCallBack(event, function(context) dev.logcall(function() func(context) end) end)
    end
end

-- Define this enviroment so it can be accessed by the custom event definition files
_G.custom_events_env = getfenv(1)

-- sim_tick must be loaded first as it is used by other custom events
require("lua_scripts.custom_events.sim_tick")

-- load the rest
require("lua_scripts.custom_events.character_moved")
require("lua_scripts.custom_events.force_unitcount_changed")

-- Logging
dev.log("custom_events.lua loaded")