--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- battle_scripting.lua
-- Activates custom battlefield so campaign battles can be scripted
-- Note this only works for campaign battles, not custom battles

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

-- Includes
local dev = require "lua_scripts.dev"
local events = require "lua_scripts.custom_events"

function activateLandGenericBattlefield()
    -- add generic battle
    cm:add_custom_battlefield(
        "generic_land",                         -- string identifier
        0,                                      -- x co-ord
        0,                                      -- y co-ord
        5000,                                   -- radius around position
        false,                                  -- will campaign be dumped
        "",                                     -- loading override
        "lua_scripts/battle_generic_land.lua",  -- script override
        "",                                     -- entire battle override
        0,                                      -- human alliance when battle override
        false,                                  -- launch battle immediately
        true                                    -- is land battle (only for launch battle immediately)
    )
end

function activateNavalGenericBattlefield()
    -- add generic battle
    cm:add_custom_battlefield(
        "generic_naval",                        -- string identifier
        0,                                      -- x co-ord
        0,                                      -- y co-ord
        5000,                                   -- radius around position
        false,                                  -- will campaign be dumped
        "",                                     -- loading override
        "lua_scripts/battle_generic_naval.lua", -- script override
        "",                                     -- entire battle override
        0,                                      -- human alliance when battle override
        false,                                  -- launch battle immediately
        false                                   -- is land battle (only for launch battle immediately)
    )
end

function pendingBattle(context)
    local battle = context:pending_battle()
    
    local attacker = battle:attacker()
    local defender = battle:defender()
    
    if not attacker:has_region() and not defender:has_region() then
        -- Neither attacker nor defender has a region, so this must be a naval battle
        --dev.log("Adding generic naval battle script")
        activateNavalGenericBattlefield()
    else
        --dev.log("Adding generic land battle script")
        activateLandGenericBattlefield()
    end
end

function removeScripts()
    -- Removing a custom battlefield that doesn't exist does nothing, so we don't need extra checks here
    --dev.log("removing generic battle scripts")
    cm:remove_custom_battlefield("generic_land")
    cm:remove_custom_battlefield("generic_naval")
end

function checkPostBattle(context) -- This is called upon a retreat, as well as the battle end, as retreats count as battles
    removeScripts()
end

-- Callbacks
events.AddEventCallBack("PendingBattle", pendingBattle)
events.AddEventCallBack("BattleCompleted", checkPostBattle)

-- Logging
dev.log("battle_scripting.lua loaded")